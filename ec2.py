#!/usr/bin/python3

import math, random, sys, json
from statistics import mean, stdev

input = sys.stdin.read()
event = json.loads(input)


dt = eval(event['key1'])
close = eval(event['key2'])
buy = eval(event['key3'])
sell = eval(event['key4'])
h = int(event['key5'])
d = int(event['key6'])
t = event['key7']
minhistory = h
shots = d
var_95_list = []
var_99_list = []
dates = []

for i in range(minhistory, len(close)):
	if t == "sell":
		if sell[i] == 1: #If it is to sell
			closing = close[i-minhistory:i]
			percent_change = [(closing[i] - closing[i-1]) / closing[i-1] for i in range(1,len(closing))]
			mean_value = mean(percent_change)
			std_value = stdev(percent_change)
			# generate much larger random number series with same broad characteristics 
			simulated = [random.gauss(mean_value,std_value) for x in range(shots)]
			# sort and pick 95% and 99%  - not distinguishing long/short risks here
			simulated.sort(reverse=True)
			var95 = simulated[int(len(simulated)*0.95)]
			var99 = simulated[int(len(simulated)*0.99)]
			var_95_list.append(var95)
			var_99_list.append(var99)
			dates.append(str(dt[i]))
	elif t == "buy":
		if buy[i] == 1: #If it is to buy
			closing = close[i-minhistory:i]
			percent_change = [(closing[i] - closing[i-1]) / closing[i-1] for i in range(1,len(closing))]
			mean_value = mean(percent_change)
			std_value = stdev(percent_change)
			# generate much larger random number series with same broad characteristics 
			simulated = [random.gauss(mean_value,std_value) for x in range(shots)]
			# sort and pick 95% and 99%  - not distinguishing long/short risks here
			simulated.sort(reverse=True)
			var95 = simulated[int(len(simulated)*0.95)]
			var99 = simulated[int(len(simulated)*0.99)]
			var_95_list.append(var95)
			var_99_list.append(var99)
			dates.append(str(dt[i]))

result = {"dates" : dates,
		"var95" : var_95_list,
		"var99" : var_99_list
		}

final_output = json.dumps(result)

print("Content-Type: application/json")
print()
print(final_output)
